# DPS-800GB-Enclosure
A Laser Cut Enclosure for a DPS-800GB PSU to be used to power 4 Lipo battery chargers, or other high current  devices

Full documentation is available [here](https://docs.google.com/document/d/1Ur0BUq02JjXGPAQBrKCh_nJV2zqGFXDlJapaJ1ta1Rg/edit?usp=sharing).


